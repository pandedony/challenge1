# RESTful API Authentication

Materi kali ini, kita akan membuat sebuah RESTful API sederhana yang dapat memberikan user hak akses ke endpoint - endpoint yang lainnya. Sistem tersebut dikenal dengan istilah `login`.

Endpoint ini menggunakan prefix `/api/{versi}`.

Mari kita breakdown fitur - fitur yang dimiliki oleh sistem ini.

- `POST` /auth/login

Payload

```json
{
  "username": "email@address.com",
  "password": "secretPassword"
}
```

Response

```json
{
  "accessToken": "secretToken",
  "accessTokenExpired": "2022/02/01T22:30:10.000Z",
  "refreshToken": "refreshToken",
  "refreshTokenExpired": "2022/02/01T22:30:10.000Z"
}
```

- `GET` /me

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
{
  ...userData
}
```

- `GET` /users

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
[
  { ...userData },
  { ...userData }
]
```

## Credentials

Username: `porsche.huel@email.com`
Password: `password123`

Username: `julius.oconner@email.com`
Password: `password123456`

Username: `melodee.dooley@email.com`
Password: `password123456789`
