// Code file untuk menjalankan postman
const express = require("express");
const server = require("./src/app");


server(express()).listen(8000, () => {
	console.log("App is tunning at http://localhost:8000")
});



// Code untuk file berupa EJS
// const express = require("express");
// const app = express();
// const route = express.Router();
// const server = require("./src/app");

// app.set("view engine", "ejs");
// app.use(express.static("public"));

// route.get("/", (req, res) => {
// 	res.render("index");
// });

// route.get("/games", (req, res) => {
// 	res.render("games/index");
// });

// app.use(route);

// app.listen(8000, () => {
// 	console.log("App is tunning at http://localhost:8000")
// });
