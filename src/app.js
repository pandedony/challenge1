const express = require("express");
const logger = require("pino-http");
const route = require("./route");
const dotenv = require("dotenv");

const server = (app) => {
	dotenv.config();
	
	// Middleware Logging
	app.use(logger());

	app.use(express.json());
	app.use(express.urlencoded({ extended : true}));

	app.use("/api/v1", route);

	return app;
};

module.exports = server;