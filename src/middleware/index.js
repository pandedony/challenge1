const jwt = require("jsonwebtoken");

module.exports = {
	isAuthenticated: (req, res, next) => {
		const accessToken = req.headers.authorization;
	
		if (!accessToken){
			return res.status(404).json({ error: "Page not Found"});
		}
	
		try {
			const isVerified = jwt.verify(accessToken, process.env.JWT_SECRET);
			if (!isVerified){
				return res.status(404).json({ error: "Page not Found"});
			}
	
			req.session = jwt.decode(accessToken);
	
			next();
		} catch (error) {
			return res.status(404).json({ error: "Page not Found"});
		}
	},
};