const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { addHours, addMinutes} = require("date-fns");

const db = require("./db/users.json");
const { isAuthenticated } = require("./middleware");
const route = express.Router();


// Alur Login
route.post("/auth/login", (req, res, next) => {
	const { username, password } = req.body;

	// Username dan password tidak boleh kosong
	if(!username && !password){
		return res.status(404).json({ error: "Missing Fields! username & password are required"});
	}

	// Mencari username
	const user = db.find((user) => user.email === username);
	if (!user){
		return res.status(404).json({ error: "User not found!"});
	}

	// Compare password 
	const isValid = bcrypt.compareSync(password, user.password);
	if (!isValid){
		return res.status(404).json({ error: "Username & password doesn't match"});
	}

	req.user = user;

	next();
}, (req, res) => {
	const { username, email } = req.user;

	const loggedInAt = Date.now();
	const accessToken = jwt.sign({
		loggedInAt, 
		user: { username, email },
	}, 
	process.env.JWT_SECRET
	);
	const accessTokenExpiredAt = addHours(loggedInAt, 1);
	const refreshToken = jwt.sign({
		loggedInAt
	}, process.env.JWT_SECRET);
	const refreshTokenExpiredAt = addMinutes(loggedInAt, 50);

	res.json({
		accessToken,
		accessTokenExpiredAt,
		refreshToken,
		refreshTokenExpiredAt,
	});
});

// Protected Endpoint
route.get("/me", isAuthenticated, (req, res) => {
	const user = db.find((user) => user.username === req.session.user.username);
	res.json(user);
});
route.get("/users", isAuthenticated, (req, res) => {
	res.json({});
});

module.exports = route;